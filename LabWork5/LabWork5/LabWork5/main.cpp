#include <vector>

#include <iostream>
#include <cstdio>
#include "Windows.h"
#include "libxl.h"
#include <string>

using namespace libxl;

int N = 0;
int M = 0;

std::vector< std::vector<double> > matrix(N, std::vector<double>(M, 0.0));
std::vector<int> aBasis(M);
std::vector<double> bBasis(M);

int deltaMin = 0;
int tMin = 0;

bool DEBUG = false;
bool NEWXL = true;
bool SAVEFLAG = false;

void saveXLFile(std::wstring fileName, int count, std::vector<double> c, std::vector<double> cBasis, double zB, std::vector<double> delta, std::vector<double> z, std::vector<double> t, int deltaMin, int tMin)
{
	Book* book = xlCreateBook(); // xlCreateXMLBook() for xlsx
	
	if (NEWXL)
	{
		DeleteFile(fileName.c_str());
		NEWXL = false;
	}
	else
		book->load(fileName.c_str());

	std::string tempStr = "Step " + std::to_string(count);
	std::wstring wStr = std::wstring(tempStr.begin(), tempStr.end());
	Sheet* sheet = book->addSheet(wStr.c_str());
		
	if (sheet)
	{
		Format* strFormat = book->addFormat();
		strFormat->setNumFormat(NUMFORMAT_TEXT);
		strFormat->setAlignH(ALIGNH_CENTER);
		strFormat->setAlignV(ALIGNV_CENTER);
		strFormat->setBorder(BORDERSTYLE_THIN);

		Font* strFont = book->addFont();
		strFont->setBold(true);
		strFormat->setFont(strFont);

		Format* numFormat = book->addFormat();
		numFormat->setNumFormat(NUMFORMAT_NUMBER_D2);
		numFormat->setAlignH(ALIGNH_CENTER);
		numFormat->setAlignV(ALIGNV_CENTER);
		numFormat->setBorder(BORDERSTYLE_THIN);

		sheet->writeStr(1, 2, L"c", strFormat);
		sheet->writeStr(2, 3, L"b", strFormat);
		sheet->writeStr(1, 3 + N + 1, L"t", strFormat);
		sheet->writeStr(2 + M + 1, 1, L"z", strFormat);
		sheet->writeStr(2 + M + 2, 1, L"delta", strFormat);

		sheet->writeNum(2 + M + 1, 3, zB, numFormat);
		sheet->writeStr(2 + M + 3, 3 + deltaMin + 1, L"+", strFormat);
		sheet->writeStr(2 + tMin + 1, 3 + N + 2, L"+", strFormat);

		for (int i = 1; i <= N; i++)
		{
			std::string tempStr = "A" + std::to_string(i);
			std::wstring wStr = std::wstring(tempStr.begin(), tempStr.end());
			sheet->writeStr(2, i + 3, wStr.c_str(), strFormat);
		};

		for (int i = 1; i <= M; i++)
		{
			std::string tempStr = "A" + std::to_string(aBasis[i - 1] + 1);
			std::wstring wStr = std::wstring(tempStr.begin(), tempStr.end());
			sheet->writeStr(i + 2, 1, wStr.c_str(), strFormat);
		};

		for (int row = 1; row <= M; row++)
		{
			sheet->writeNum(2 + row, 2, cBasis[row - 1], numFormat);
			sheet->writeNum(2 + row, 3, bBasis[row - 1], numFormat);
			sheet->writeNum(2 + row, 3 + N + 1, t[row - 1], numFormat);

			for (int column = 1; column <= N; column++)
				sheet->writeNum(2 + row, 3 + column, matrix[row - 1][column - 1], numFormat);
		};
			
		for (int column = 1; column <= N; column++)
		{
			sheet->writeNum(1, 3 + column, c[column - 1], numFormat);
			sheet->writeNum(2 + M + 1, 3 + column, z[column - 1], numFormat);
			sheet->writeNum(2 + M + 2, 3 + column, delta[column - 1], numFormat);
		};
	};

	book->save(fileName.c_str());
	book->release();
};

void recalcMatrix(const std::vector<double> c, std::vector<int>& aTemp, std::vector<double>& bTemp, std::vector<double>& cTemp, std::vector< std::vector<double> >& matrixTemp)
{
	aTemp[tMin] = deltaMin;
	cTemp[tMin] = c[deltaMin];

	for (int row = 0; row < M; row++)
		if (row != tMin)
			bTemp[row] = bBasis[row] - bBasis[tMin] * matrix[row][deltaMin] / matrix[tMin][deltaMin];
	bTemp[tMin] = bBasis[tMin] / matrix[tMin][deltaMin];

	for (int row = 0; row < M; row++)
		for (int column = 0; column < N; column++)
		{
			if ((row != tMin) && (column != deltaMin))
				matrixTemp[row][column] = matrix[row][column] - matrix[row][deltaMin] * matrix[tMin][column] / matrix[tMin][deltaMin];
			if ((row == tMin) && (column != deltaMin))
				matrixTemp[row][column] = matrix[tMin][column] / matrix[tMin][deltaMin];
			if ((row != tMin) && (column == deltaMin))
				matrixTemp[row][column] = 0.0;
			if ((row == tMin) && (column == deltaMin))
				matrixTemp[row][column] = 1.0;
		};
};

std::vector<double> runSimplexMethod(std::vector< std::vector<double> > a, std::vector<double> b, std::vector<double> c, std::vector<int> indBasis)
{
	std::vector< std::vector<double> > matrixTemp = a;
	std::vector<int> aTemp = indBasis;
	std::vector<double> bTemp = b;
	std::vector<double> cTemp;
	std::vector<double> returnVec;
	for (std::size_t i = 0; i < indBasis.size(); i++)
		cTemp.push_back(c[indBasis[i]]);

	bool FLAG_WHILE = true;
	bool FLAG_ANSWER = true;
	int count = 1;
	do
	{
		FLAG_WHILE = true;

		std::vector<double> cBasis(M);

		matrix = matrixTemp;
		aBasis = aTemp;
		cBasis = cTemp;
		bBasis = bTemp;

		// ������ ����� c[i] * b[i] 
		double zB = 0.0;
		for (int i = 0; i < M; i++)
			zB += cBasis[i] * bBasis[i];

		// �������������� ������� ��� ����������
		std::vector<double> z(N, 0.0);
		std::vector<double> delta(N, 0.0);
		std::vector<double> t(M, 0.0);

		// ���������� z
		for (int column = 0; column < N; column++)
			for (int row = 0; row < M; row++)
				z[column] += matrix[row][column] * cBasis[row];

		// ���������� delta
		for (int i = 0; i < N; i++)
			delta[i] = z[i] - c[i];

		// ���������� ������������ delta
		deltaMin = 0;
		for (int i = 0; i < N; i++)
			if (delta[i] <= delta[deltaMin])
				deltaMin = i;
		returnVec = delta;

		// ������� ������ �� �����
		for (int i = 0; i < N; i++)
		{
			FLAG_ANSWER = true;

			for (int column = 0; column < N; column++)
				if (delta[column] < 0)
				{
					FLAG_ANSWER = false;
					break;
				};

			if (FLAG_ANSWER)
			{
				std::cout.precision(8);
				if (SAVEFLAG) saveXLFile(L"Simplex.xls", count, c, cBasis, zB, delta, z, t, 0, 0);

				for (int column = 0; column < N; column++)
				{
					bool flag = true;
					for (int row = 0; row < M; row++)
					{
						if (aBasis[row] == column)
						{
							std::cout << "x[" << column + 1 << "] = " << bBasis[row] << std::endl;
							flag = false;
							break;
						};
					};

					if (flag)
						std::cout << "x[" << column + 1 << "] = " << 0.0 << std::endl;
				};

				std::cout << "Func = " << zB << std::endl;

				FLAG_WHILE = false;
			};
			if (!FLAG_WHILE) break;
		};
		if (!FLAG_WHILE) break;

		// ���������� t
		tMin = 0;
		for (int i = 0; i < M; i++)
		{
			t[i] = bBasis[i] / matrix[i][deltaMin];
			if (t[i] >= 0.0)
				tMin = i;
		};

		// ���������� ������������ t
		for (int i = 0; i < M; i++)
			if ((t[i] <= t[tMin]) && (t[i] >= 0.0))
				tMin = i;

		// ���������� ������� ������������ � ����
		if (SAVEFLAG) saveXLFile(L"Simplex.xls", count, c, cBasis, zB, delta, z, t, deltaMin, tMin);
		count++;

		// ���������� ������� ������������
		recalcMatrix(c, aTemp, bTemp, cTemp, matrixTemp);

		if (DEBUG) FLAG_WHILE = false;
	} while (FLAG_WHILE);

	return returnVec;
};

int main()
{
	// ��������� ������

	N = 8;
	M = 4;

	std::vector< std::vector<double> > a = {
	{ 101.0, 101.0, 945.0, 1500.0, 1.0, 0.0, 0.0, 0.0 },
	{   6.0,   5.0,   0.0,    0.0, 0.0, 1.0, 0.0, 0.0 },
	{   0.0,   0.0,   1.0,    0.0, 0.0, 0.0, 1.0, 0.0 },
	{   0.0,   0.0,   0.0,    1.0, 0.0, 0.0, 0.0, 1.0 }
	};
	std::vector<double> b = { 1400000.0, 63000.0, 480.0, 320.0 };
	std::vector<double> c = { 24.0, 27.0, 138.0, 72.0, 0.0, 0.0, 0.0, 0.0 };
	std::vector<int> indBasis = { 4, 5, 6, 7 };

	/*N = 5;
	M = 2;

	std::vector< std::vector<double> > a = {
		{ 3.0,  2.0,  1.0, 1.0, 0.0 },
		{ -1.0, 2.0, -1.0, 0.0, 1.0 }
	};

	std::vector<double> b = { 14.0, 4.0 };
	std::vector<double> c = { 1.0, -1.0, 0.0, 0.0, -1.0 };
	std::vector<int> indBasis = { 3, 4 };*/

	/*N = 3;
	M = 1;

	std::vector< std::vector<double> > a = {
		{ 15.0, 30.0, 1.0 }
	};

	std::vector<double> b = { 96.0 };
	std::vector<double> c = { 2.0, 1.0, 0.0 };
	std::vector<int> indBasis = { 2 };*/

	std::vector<double> delta = runSimplexMethod(a, b, c, indBasis);

	while (true)
	{
		// ���������� ��� ������� modf
		double numTemp;

		// �������� ��������������� ���� ��������
		bool FLAG_WHILE = false;
		for (std::size_t row = 0; row < bBasis.size(); row++)
		{
			double tempNumber = modf(bBasis[row], &numTemp);
			if (tempNumber < 0.0) tempNumber = 1.0 - abs(tempNumber);
			if ((aBasis[row] < 4) && (tempNumber >= 1.0E-8) && ((1.0 - tempNumber) >= 1.0E-8))
				FLAG_WHILE = true;
		};
		if (!FLAG_WHILE)
			break;

		// ����� x_i � ������������ ������� ������
		int maxInd = 0;
		for (std::size_t row = 0; row < bBasis.size(); row++)
			if ((aBasis[row] < 4) && (modf(bBasis[maxInd], &numTemp) <= modf(bBasis[row], &numTemp)))
				maxInd = row;

		// ����� ����� �������� ����������
		int deltaAbsMin;
		std::vector<bool> flagDelta(delta.size(), true);
		for (std::size_t column = 0; column < delta.size(); column++)
		{
			for (std::size_t row = 0; row < aBasis.size(); row++)
				if (column == aBasis[row])
				{
					flagDelta[column] = false;
					break;
				};

			if (abs(delta[column]) > 0.0)
				deltaAbsMin = column;
		};

		for (std::size_t column = 0; column < delta.size(); column++)
			if (flagDelta[column] && (delta[deltaAbsMin] > delta[column]) && (delta[column] > 0.0))
				deltaAbsMin = column;

		// ���������� ������ �����������
		for (std::size_t row = 0; row < matrix.size(); row++)
			matrix[row].push_back(0.0);

		std::vector<double> tempVec;
		for (int column = 0; column < N; column++)
		{
			double temp = modf(matrix[maxInd][column], &numTemp);
			temp = (temp < 0.0) ? 1.0 + temp : temp;
			tempVec.push_back((flagDelta[column]) ? temp : 0.0);
		};
		tempVec.push_back(-1.0);
		matrix.push_back(tempVec);

		deltaMin = deltaAbsMin;
		tMin = M;

		// �������� ������������, ������� � ����
		for (int i = 0; i < matrix.size(); i++)
			for (int j = 0; j < matrix[0].size(); j++)
				if (abs(matrix[i][j]) < 1.0E-8)
					matrix[i][j] = 0.0;

		// ���������� ������� ������������
		aBasis.push_back(deltaAbsMin);
		bBasis.push_back(modf(bBasis[maxInd], &numTemp));
		c.push_back(0.0);
		N++;
		M++;

		// ���������� ������� ������������
		std::vector<int> aTemp(aBasis);
		std::vector<double> bTemp(bBasis);
		std::vector<double> cTemp;
		for (std::size_t i = 0; i < aBasis.size(); i++)
			cTemp.push_back(c[aBasis[i]]);
		std::vector< std::vector<double> > matrixTemp(matrix);

		NEWXL = true;
		if (SAVEFLAG) saveXLFile(L"Temp.xls", 0, c, cTemp, 0.0, std::vector<double>(N, 0.0), std::vector<double>(N, 0.0), std::vector<double>(M, 0.0), deltaMin, tMin);

		recalcMatrix(c, aTemp, bTemp, cTemp, matrixTemp);

		std::cout << "Next number: " << aBasis[maxInd] + 1 << std::endl;

		//������ ��������-�������
		matrix = matrixTemp;
		aBasis = aTemp;
		bBasis = bTemp;
		delta = runSimplexMethod(matrix, bBasis, c, aBasis);
	};

	return 0;
};