#include <vector>

#include <iostream>
#include "libxl.h"
#include <string>

using namespace libxl;

int N = 8;
int M = 4;

bool DEBUG = false;

void saveXLFile(int count, std::vector < std::vector<double> > matrix, std::vector<double> c, std::vector<int> aBasis, std::vector<double> bBasis, std::vector<double> cBasis, double zB, std::vector<double> delta, std::vector<double> z, std::vector<double> t, int deltaMin, int tMin)
{
	Book* book = xlCreateBook(); // xlCreateXMLBook() for xlsx
	if (book->load(L"Simplex.xls"))
	{
		std::string tempStr = "Step " + std::to_string(count);
		std::wstring wStr = std::wstring(tempStr.begin(), tempStr.end());
		Sheet* sheet = book->addSheet(wStr.c_str());
		
		if (sheet)
		{
			Format* strFormat = book->addFormat();
			strFormat->setNumFormat(NUMFORMAT_TEXT);
			strFormat->setAlignH(ALIGNH_CENTER);
			strFormat->setAlignV(ALIGNV_CENTER);
			strFormat->setBorder(BORDERSTYLE_THIN);

			Font* strFont = book->addFont();
			strFont->setBold(true);
			strFormat->setFont(strFont);

			Format* numFormat = book->addFormat();
			numFormat->setNumFormat(NUMFORMAT_NUMBER_D2);
			numFormat->setAlignH(ALIGNH_CENTER);
			numFormat->setAlignV(ALIGNV_CENTER);
			numFormat->setBorder(BORDERSTYLE_THIN);

			sheet->writeStr(1, 2, L"c", strFormat);
			sheet->writeStr(2, 3, L"b", strFormat);
			sheet->writeStr(1, 3 + N + 1, L"t", strFormat);
			sheet->writeStr(2 + M + 1, 1, L"z", strFormat);
			sheet->writeStr(2 + M + 2, 1, L"delta", strFormat);

			sheet->writeNum(2 + M + 1, 3, zB, numFormat);
			sheet->writeStr(2 + M + 3, 3 + deltaMin + 1, L"+", strFormat);
			sheet->writeStr(2 + tMin + 1, 3 + N + 2, L"+", strFormat);

			for (int i = 1; i <= N; i++)
			{
				std::string tempStr = "A" + std::to_string(i);
				std::wstring wStr = std::wstring(tempStr.begin(), tempStr.end());
				sheet->writeStr(2, i + 3, wStr.c_str(), strFormat);
			};

			for (int i = 1; i <= M; i++)
			{
				std::string tempStr = "A" + std::to_string(aBasis[i - 1] + 1);
				std::wstring wStr = std::wstring(tempStr.begin(), tempStr.end());
				sheet->writeStr(i + 2, 1, wStr.c_str(), strFormat);
			};

			for (int row = 1; row <= M; row++)
			{
				sheet->writeNum(2 + row, 2, cBasis[row - 1], numFormat);
				sheet->writeNum(2 + row, 3, bBasis[row - 1], numFormat);
				sheet->writeNum(2 + row, 3 + N + 1, t[row - 1], numFormat);

				for (int column = 1; column <= N; column++)
					sheet->writeNum(2 + row, 3 + column, matrix[row - 1][column - 1], numFormat);
			};
			
			for (int column = 1; column <= N; column++)
			{
				sheet->writeNum(1, 3 + column, c[column - 1], numFormat);
				sheet->writeNum(2 + M + 1, 3 + column, z[column - 1], numFormat);
				sheet->writeNum(2 + M + 2, 3 + column, delta[column - 1], numFormat);
			};
		};

		book->save(L"Simplex.xls");
		book->release();
	};
};

int main()
{
	// ��������� ������

	/*N = 5; M = 2;
	std::vector< std::vector<double> > a = {
		{ 9.0, 3.0, 1.0, 1.0, 0.0 },
		{ 1.0, 4.0, 5.0, 0.0, 1.0 }
	};
	std::vector<double> b = { 4.0, 2.0 };
	std::vector<double> c = { 15.0, 27.0, 20.0, 0.0, 0.0 };
	std::vector<int> indBasis = { 3, 4 };
	std::vector<double> cTemp = { c[indBasis[0]], c[indBasis[1]] };*/

	/*N = 8; M = 4;
	std::vector< std::vector<double> > a = {
		{ 1.01,        1.01,        9.45,       0.3,         1.0, 0.0, 0.0, 0.0 },
		{ 1.0 / 500.0, 1.0 / 600.0, 0.0,        1.0 / 600.0, 0.0, 1.0, 0.0, 0.0 },
		{ 0.0,         0.0,         1.0 / 30.0, 0.0,         0.0, 0.0, 1.0, 0.0 },
		{ 0.0,         1.0,         0.0,        1.0,         0.0, 0.0, 0.0, 1.0 }
	};
	std::vector<double> b = { 14000.0, 21.0, 16.0, 40.0 };
	std::vector<double> c = { 24.0, 27.0, 138.0, 7.7, 0.0, 0.0, 0.0, 0.0 };
	std::vector<int> indBasis = { 4, 5, 6, 7 };
	std::vector<double> cTemp = { c[indBasis[0]], c[indBasis[1]], c[indBasis[2]], c[indBasis[3]] };*/

	/*std::vector< std::vector<double> > a = {
		{ 1.01,        1.01,        9.45,       15.0,        1.0, 0.0, 0.0, 0.0 },
		{ 1.0 / 500.0, 1.0 / 600.0, 0.0,        0.0,         0.0, 1.0, 0.0, 0.0 },
		{ 0.0,         0.0,         1.0 / 30.0, 0.0,         0.0, 0.0, 1.0, 0.0 },
		{ 0.0,         0.0,         0.0,        1.0 / 20.0,  0.0, 0.0, 0.0, 1.0 }
	};
	std::vector<double> b = { 14000.0, 21.0, 16.0, 16.0 };
	std::vector<double> c = { 24.0, 27.0, 138.0, 72.0, 0.0, 0.0, 0.0, 0.0 };
	std::vector<int> indBasis = { 4, 5, 6, 7 };
	std::vector<double> cTemp = { c[indBasis[0]], c[indBasis[1]], c[indBasis[2]], c[indBasis[3]] };*/

	std::vector< std::vector<double> > a = {
		{ 1.01, 1.0 / 500.0,        0.0,        0.0, 1.0, 0.0, 0.0, 0.0 },
		{ 1.01, 1.0 / 600.0,        0.0,        0.0, 0.0, 1.0, 0.0, 0.0 },
		{ 9.45,         0.0, 1.0 / 30.0,        0.0, 0.0, 0.0, 1.0, 0.0 },
		{ 15.0,         0.0,        0.0, 1.0 / 20.0, 0.0, 0.0, 0.0, 1.0 }
	};
	std::vector<double> b = { 24.0, 27.0, 138.0, 72.0 };
	std::vector<double> c = { 14000.0, 21.0, 16.0, 16.0, 0.0, 0.0, 0.0, 0.0 };
	std::vector<int> indBasis = { 4, 5, 6, 7 };
	std::vector<double> cTemp = { c[indBasis[0]], c[indBasis[1]], c[indBasis[2]], c[indBasis[3]] };

	std::vector< std::vector<double> > matrixTemp = a;
	std::vector<int> aTemp = indBasis;
	std::vector<double> bTemp = b;

	bool FLAG_WHILE = true;
	bool FLAG_ANSWER = true;
	int count = 1;
	do
	{
		FLAG_WHILE = true;

		std::vector< std::vector<double> > matrix(N, std::vector<double>(M, 0.0));
		std::vector<int> aBasis(M);
		std::vector<double> cBasis(M);
		std::vector<double> bBasis(M);

		matrix = matrixTemp;
		aBasis = aTemp;
		cBasis = cTemp;
		bBasis = bTemp;

		// ������ ����� c[i] * b[i] 
		double zB = 0.0;
		for (int i = 0; i < M; i++)
			zB += cBasis[i] * bBasis[i];

		// �������������� ������� ��� ����������
		std::vector<double> z(N, 0.0);
		std::vector<double> delta(N, 0.0);
		std::vector<double> t(M, 0.0);

		// ���������� z
		for (int column = 0; column < N; column++)
			for (int row = 0; row < M; row++)
				z[column] += matrix[row][column] * cBasis[row];

		// ���������� delta
		for (int i = 0; i < N; i++)
			delta[i] = z[i] - c[i];

		// ���������� ������������ delta
		int deltaMin = 0;
		for (int i = 0; i < N; i++)
			if (delta[i] <= delta[deltaMin])
				deltaMin = i;

		// ������� ������ �� �����
		for (int i = 0; i < N; i++)
		{
			FLAG_ANSWER = true;

			for (int column = 0; column < N; column++)
				if (delta[column] < 0)
				{
					FLAG_ANSWER = false;
					break;
				};

			if (FLAG_ANSWER)
			{
				std::cout.precision(8);
				saveXLFile(count, matrix, c, aBasis, bBasis, cBasis, zB, delta, z, t, 0, 0);

				for (int column = 0; column < N; column++)
				{
					bool flag = true;
					for (int row = 0; row < M; row++)
					{
						if (aBasis[row] == column)
						{
							std::cout << "x[" << column + 1 << "] = " << bBasis[row] << std::endl;
							flag = false;
							break;
						};
					};

					if (flag)
						std::cout << "x[" << column + 1 << "] = " << 0.0 << std::endl;
				};

				std::cout << "Func = " << zB << std::endl;
				
				FLAG_WHILE = false;
			};
			if (!FLAG_WHILE) break;
		};
		if (!FLAG_WHILE) break;

		// ���������� t
		int tMin = 0;
		for (int i = 0; i < M; i++)
		{
			t[i] = bBasis[i] / matrix[i][deltaMin];
			if (t[i] >= 0.0)
				tMin = i;
		};

		// ���������� ������������ t
		for (int i = 0; i < M; i++)
			if ((t[i] <= t[tMin]) && (t[i] >= 0.0))
				tMin = i;

		// ���������� ������� ������������ � ����
		saveXLFile(count, matrix, c, aBasis, bBasis, cBasis, zB, delta, z, t, deltaMin, tMin);
		count++;

		// ���������� ������� ������������
		aTemp[tMin] = deltaMin;
		cTemp[tMin] = c[deltaMin];

		for (int row = 0; row < M; row++)
			if (row != tMin)
				bTemp[row] = bBasis[row] - bBasis[tMin] * matrix[row][deltaMin] / matrix[tMin][deltaMin];
		bTemp[tMin] = bBasis[tMin] / matrix[tMin][deltaMin];

		for (int row = 0; row < M; row++)
			for (int column = 0; column < N; column++)
			{
				if ((row != tMin) && (column != deltaMin))
					matrixTemp[row][column] = matrix[row][column] - matrix[row][deltaMin] * matrix[tMin][column] / matrix[tMin][deltaMin];
				if ((row == tMin) && (column != deltaMin))
					matrixTemp[row][column] = matrix[tMin][column] / matrix[tMin][deltaMin];
				if ((row != tMin) && (column == deltaMin))
					matrixTemp[row][column] = 0.0;
				if ((row == tMin) && (column == deltaMin))
					matrixTemp[row][column] = 1.0;
			};

		if (DEBUG) FLAG_WHILE = false;
	} while (FLAG_WHILE);

	return 0;
};