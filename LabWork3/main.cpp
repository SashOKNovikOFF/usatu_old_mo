#include <iostream> // Для ввода/вывода данных на экран
#include <cmath>    // Для математический функций

struct Point // Координата точки
{
	Point() : x(0.0), y(0.0), z(0.0) { };                             // Конструктор
	Point(double x_, double y_, double z_) : x(x_), y(y_), z(z_) { }; // Конструктор с параметрами
	
	double x; // Координата точки x
	double y; // Координата точки y
	double z; // Координата точки z
};

double generalFunc(const Point& x); // Получить значение исходной функции в точке
double cond1(const Point& x); // Получить значение функции g_1(x) в точке
double cond2(const Point& x); // Получить значение функции g_2(x) в точке
double cond3(const Point& x); // Получить значение функции g_3(x) в точке

double fineFunc(const Point& x); // Получить значение штрафной функции в точке
double func(const Point& x);     // Получить значение функции Лагранжа в точке
Point grad(const Point& x);      // Получить значение градиента функции Лагранжа в точке
double norm(const Point& x);     // Получить значение нормы вектора в точке

Point gradientFallMethod(const Point& x0, const double epsilon, const int numIter, int& counter); // Метод градиентного спуска с дробным шагом

void OutputResult(const Point& x, int counter); // Вывод результата на экран

double fine      = 1.0;
double deltaFine = 4.0;

int main()
{
	int counter = 0;           // Счётчик
	double epsGlobal = 1.0E-4; // Точность решения задачи условной оптимизации
	double epsLocal = 1.0E-4;  // Точность решения задачи безусловной оптимизации
	int numIter = 100;         // Число итераций
	Point xOld(0.3, 0.9, 0.0); // Начальная точка x_0
	
	while ((std::abs(fineFunc(xOld)) >= epsGlobal))  // Пока не достигли заданной точности, выполняем расчёт
	{
		int temp = 0;
		Point xNew = gradientFallMethod(xOld, epsLocal, numIter, temp);
		
		Point normP(xOld.x - 2.0, xOld.y - 3.0, 0.0);
		std::cout << "Norm = " << norm(normP) << std::endl;
		std::cout << "fine = " << fine << " fineFunc = " << fineFunc(xOld) << std::endl;
		OutputResult(xOld, counter);
		
		if (std::abs(fineFunc(xNew)) >= epsGlobal)  // Если на очередном шаге превысили точность, переходим на следующую итерацию
		{
			fine = fine / deltaFine;
			xOld = xNew;
			counter++;
		};
	};
	
	Point normP(xOld.x - 2.0, xOld.y - 3.0, 0.0);
	std::cout << "Norm = " << norm(normP) << std::endl;
	std::cout << "fine = " << fine << " fineFunc = " << fineFunc(xOld) << std::endl;
	OutputResult(xOld, counter);
	
	return 0;
}

double generalFunc(const Point& x)
{
	//return 2.0 * x.x * x.x + 2.0 * x.y * x.y - x.x * x.y - x.x - 4.0 * x.y; // Вариант 4
	//return x.x * x.x - x.x - x.y;                                           // Вариант 13
	return pow(x.x - 2.0, 2.0) + pow(x.y - 3.0, 2.0);                         // Вариант 14
}
double cond1(const Point& x)
{
	//return x.y + 2.0 * x.x - 12.0;                        // Вариант 4
	//return x.x * x.x + 3.0 * x.y * x.y + 2.0 * x.x - 3.0; // Вариант 13
	return x.x + x.y - 9.0;                                 // Вариант 14
}
double cond2(const Point& x)
{
	//return - x.x;                // Вариант 4
	//return - x.x;                // Вариант 13
	return x.x + 2.0 * x.y - 12.0; // Вариант 14
}
double cond3(const Point& x)
{
	return - x.y; // Вариант 13
}

double fineFunc(const Point& x)
{
	//return fine * (1.0 / cond1(x) + 1.0 / cond2(x) + 1.0 / cond3(x)); // Вариант 13
	return fine * (1.0 / cond1(x) + 1.0 / cond2(x));                    // Вариант 4, 14
}
double func(const Point& x)
{
	return generalFunc(x) - fineFunc(x);
}
Point grad(const Point& x)
{
	// Вариант 4
	//double xTemp = 4.0 * x.x - x.y - 1.0 + fine * (2.0 / pow(cond1(x), 2.0) -  1.0 / pow(x.x, 2.0));
	//double yTemp = - x.x + 4.0 * x.y - 4.0 + fine / pow(cond1(x), 2.0);
	//double zTemp = 0.0;
	
	// Вариант 13
	//double denom = x.x * x.x + 3.0 * x.y * x.y + 2.0 * x.x - 3.0;
	//double xTemp = 2.0 * x.x - 1.0 + fine * ((2.0 * x.x + 2.0) / denom / denom - 1.0 / x.x / x.x);
	//double yTemp =           - 1.0 + fine * (        6.0 * x.y / denom / denom - 1.0 / x.y / x.y);
	//double zTemp = 0.0;
	
	// Вариант 14
	double xTemp = 2.0 * x.x - 4.0 + fine * (1.0 / pow(cond1(x), 2.0) + 1.0 / pow(cond2(x), 2.0));
	double yTemp = 2.0 * x.y - 6.0 + fine * (1.0 / pow(cond1(x), 2.0) + 2.0 / pow(cond2(x), 2.0));
	double zTemp = 0.0;
	
	Point temp(xTemp, yTemp, zTemp);
	return temp;
}
double norm(const Point& x)
{
	return sqrt(x.x * x.x + x.y * x.y + x.z * x.z);
}

Point gradientFallMethod(const Point& x0, const double epsilon, const int numIter, int& counter)
{
	Point xOld = x0;   // Старое значение точки
	Point xNew = xOld; // Новое значение точки
	double step = 1.0; // Длина шага
	
	while((pow(norm(grad(xOld)), 2.0) >= epsilon) && (counter < numIter)) // Пока не достигли заданной точности, выполняем расчёт
	{
		xNew.x = xOld.x - step * grad(xOld).x;
		xNew.y = xOld.y - step * grad(xOld).y;
		xNew.z = xOld.z - step * grad(xOld).z;
		
		if (func(xNew) < func(xOld)) // Если полученное значение функции меньше исходного, переходим на следующую итерацию
		{
			xOld = xNew;
			counter++;
		}
		else // Иначе мельчим шаг и повторяем расчёт
			step /= 2.0;
	};
	
	return xOld;
}

void OutputResult(const Point& x, int counter)
{
	std::cout << "Points(x, y, z): ";
	std::cout << "("   << x.x << ", "  << x.y << ", " << x.z << "), ";
	std::cout << "counter = " << counter << std::endl;
}