#include <iostream> // Для ввода/вывода данных на экран
#include <cmath>    // Для математический функций

struct Point // Координата точки
{
	Point() : x(0.0), y(0.0) { };                   
	// Конструктор
	Point(double x_, double y_) : x(x_), y(y_) { };
	// Конструктор с параметрами
	
	double x; // Координата точки x
	double y; // Координата точки y
};
struct Matrix // Квадратная матрица
{
	Matrix() : a(0), b(0), c(0), d(0) { };
	// Конструктор
	Matrix(double a_, double b_, double c_, double d_) :
	a(a_), b(b_), c(c_), d(d_) { }; // Конструктор с параметрами
	
	double a; // Элемент (1, 1)
	double b; // Элемент (1, 2)
	double c; // Элемент (2, 1)
	double d; // Элемент (2, 2)
};

double func(const Point& x); // Получить значение функции в точке
Point grad(const Point& x);  // Получить значение градиента
                             // функции в точке
double norm(const Point& x); // Получить значение нормы
                             // вектора в точке

Point gradientFallMethod(const double epsilon,
const int numIter, int& counter); // Метод градиентного спуска
                                  // с дробным шагом
Point markvardtMethod(const double epsilon,
const int numIter, int& counter); // Метод Маквардта

Matrix inverseMatrix(double lambda); // Получить матрицу,
                                     // обратную (H(x) +
									 // lambda * E)
Point multiplyMV(Matrix
matrix, Point vector); // Получить
                       // произведение матрицы и вектора

void OutputResult(const Point& x,
int counter); // Вывод результата на экран

int main()
{
	int counter;             // Счётчик
	double epsilon = 1.0E-2; // Точность расчётов
	int numIter = 100;       // Число итераций
	
	counter = 0;
	Point xG = gradientFallMethod(epsilon, numIter, counter);
	OutputResult(xG, counter);
	
	counter = 0;
	Point xM = markvardtMethod(epsilon, numIter, counter);
	OutputResult(xM, counter);

	return 0;
}

double func(const Point& x)
{
	return (x.x + x.y) * (x.x + x.y) + (x.y + 2.0) *
	(x.y + 2.0);
}
Point grad(const Point& x)
{
	Point temp(2.0 * (x.x + x.y), 2 * x.x + 4 * x.y + 4);
	return temp;
}
double norm(const Point& x)
{
	return sqrt(x.x * x.x + x.y * x.y);
}

Point gradientFallMethod(const double epsilon,
const int numIter, int& counter)
{
	Point xOld(0.0, 0.0); // Старое значение точки
	Point xNew = xOld;    // Новое значение точки
	double step = 1.0;    // Длина шага
	
	while((pow(norm(grad(xOld)), 2.0) >= epsilon)
	&& (counter < numIter)) // Пока не достигли заданной
                            // точности, выполняем расчёт
	{
		xNew.x = xOld.x - step * grad(xOld).x;
		xNew.y = xOld.y - step * grad(xOld).y;
		
		if (func(xNew) < func(xOld)) // Если полученное
		                             // значение функции меньше
									 // исходного, переходим на
									 // следующую итерацию
		{
			xOld = xNew;
			counter++;
		}
		else // Иначе мельчим шаг и повторяем расчёт
			step /= 2.0;
	};
	
	return xOld;
}

Matrix inverseMatrix(double lambda)
{
	double denom = lambda * lambda + 6.0 * lambda + 4.0;
	
	Matrix temp((4.0 + lambda) / denom, -2.0 / denom, -2.0
	/ denom, (2.0 + lambda) / denom);
	return temp;
}
Point multiplyMV(Matrix matrix, Point vector)
{
	Point temp(matrix.a * vector.x + matrix.b * vector.y,
	matrix.c * vector.x + matrix.d * vector.y);
	return temp;
}
Point markvardtMethod(const double epsilon,
const intnumIter, int& counter)
{
	Point xOld(0.0, 0.0);  // Старое значение точки
	Point xNew = xOld;     // Новое значение точки
	double lambda = 1.0E4; // Параметр
	
	while((norm(grad(xOld)) >= epsilon)
	&& (counter < numIter)) // Пока не достигли заданной
                            // точности, выполняем расчёт
	{
		Point delta = multiplyMV(inverseMatrix(lambda),
		grad(xOld));
		xNew.x = xOld.x - delta.x;
		xNew.y = xOld.y - delta.y;
		
		if (func(xNew) < func(xOld)) // Если полученное
		                             // значение функции
									 // меньше исходного,
									 // переходим на 
									 // следующую итерацию
		{
			lambda = lambda / 2.0;
			xOld = xNew;
			counter++;
		}
		else // Иначе увеличиваем параметр и повторяем расчёт
			lambda = 2.0 * lambda;
	};
	
	return xOld;
}

void OutputResult(const Point& x, int counter)
{
	std::cout << "Points(x, y): ";
	std::cout << "("   << x.x << ", "  << x.y << "), ";
	std::cout << "counter = " << counter << std::endl;
}