set terminal wxt persist

set xrange [-5:6]
set yrange [-3:2]

plot "graph.dat"  using 1:2 with lines, \
     "points.dat" using 1:2 with points