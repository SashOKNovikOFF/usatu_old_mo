#include <fstream>
#include <string>
#include <iomanip>
#include <cmath>

const bool DEBUG_MAIN = true;
const bool DEBUG_VENNAN = false;
const bool DEBUG_DICHOT = false;
const bool DEBUG_GOLDEN = false;
const bool DEBUG_FIBBON = false;

const std::string fileV = "PointsV.dat";
const std::string fileD = "PointsD.dat";
const std::string fileG = "PointsG.dat";
const std::string fileF = "PointsF.dat";

const int width = 6;
const double leftBorder = -5.0;
const double rightBorder = 6.0;

double func(double x)
{
	return x < 2.0 ? 1.0 / (x * x + 1) : x * x / 2.0 - 4.0 * x + 6.0;
}

void saveGraphFile(double a, double b, double step)
{
	std::ofstream file("graph.dat", std::ios::out);
	
	file << std::setw(width) << "X" << " ";
	file << std::setw(width) << "Y" << std::endl;

	file << std::scientific;
	file << std::setprecision(width);
	for (double x = a; x <= b; x += step)
		file << x << " " << func(x) << std::endl;

	file.close();
}

void createPointFile(std::string fileName)
{
	std::ofstream file(fileName, std::ios::out);

	file << std::setw(width) << "X" << " ";
	file << std::setw(width) << "Y" << std::endl;

	file.close();
}

void addPointInFile(std::string fileName, double x, int iteration)
{
	std::ofstream file(fileName, std::ios::app);

	file << std::scientific;
	file << std::setprecision(width);
	file << x << " " << func(x) << " " << iteration << std::endl;

	file.close();
}

void addDataInFile(std::string fileName, std::string data, double x)
{
	std::ofstream file(fileName, std::ios::app);

	file << std::scientific;
	file << std::setprecision(width);
	file << data << " = " << x << std::endl;

	file.close();
}

int VennanMethod(double& a, double& b, double xOld, double step)
{
	int count = 0;

	if (DEBUG_VENNAN) createPointFile(fileV);

	double fLeft = func(xOld - step);
	double fCenter = func(xOld);
	double fRight = func(xOld + step);

	if ((fLeft <= fCenter) && (fCenter >= fRight))
		return -1;
	if ((fLeft >= fCenter) && (fCenter <= fRight))
		return -2;

	double delta;
	double xNew = xOld;
	if ((fLeft >= fCenter) && (fCenter >= fRight))
	{
		a = xOld;
		xNew += step;
		delta = step;
	}
	else
	{
		b = xOld;
		xNew -= step;
		delta = -step;
	};
	count++;

	if (DEBUG_VENNAN) addPointInFile(fileV, xOld, -1);
	if (DEBUG_VENNAN) addPointInFile(fileV, xNew, -1);

	while (true)
	{
		xOld = xNew;
		xNew = xOld + pow(2.0, count) * delta;
		
		if (DEBUG_VENNAN) addPointInFile(fileV, xNew, -1);

		if (func(xNew) < func(xOld))
		{
			if (delta > 0.0)
				a = xOld;
			else
				b = xOld;
		}
		else
		{
			if (delta > 0.0)
				b = xNew;
			else
				a = xNew;
			break;
		};
	};
};

double DichotomyMethod(double a, double b, double epsilon, int& iteration)
{
	int count = 0;
	double ak = a;
	double bk = b;
	double ck = (ak + bk) / 2.0;
	iteration = -1;

	if (DEBUG_DICHOT) addDataInFile(fileD, "ak", ak);
	if (DEBUG_DICHOT) addDataInFile(fileD, "bk", bk);

	while (std::abs(bk - ak) > epsilon)
	{
		iteration++;

		double yk = (ak + ck) / 2.0;
		if (func(yk) <= func(ck))
		{
			bk = ck;
			ck = yk;
		}
		else
		{
			double zk = (bk + ck) / 2.0;
			if (func(ck) <= func(zk))
			{
				ak = yk;
				bk = zk;
			}
			else
			{
				ak = ck;
				ck = zk;
			};
		};

		if (DEBUG_DICHOT) addDataInFile(fileD, "ak", ak);
		if (DEBUG_DICHOT) addDataInFile(fileD, "bk", bk);

		count++;
	};

	return (ak + bk) / 2.0;
}

double GoldenRatioMethod(double a, double b, double epsilon, int& iteration)
{
	int count = 0;
	double ak = a;
	double bk = b;
	double yk = a + (3.0 - sqrt(5.0)) / 2.0 * (b - a);
	double zk = a + (sqrt(5.0) - 1.0) / 2.0 * (b - a);
	iteration = -1;

	if (DEBUG_GOLDEN) addDataInFile(fileD, "ak", ak);
	if (DEBUG_GOLDEN) addDataInFile(fileD, "bk", bk);

	while (std::abs(bk - ak) > epsilon)
	{
		iteration++;

		if (func(yk) <= func(zk))
		{
			bk = zk;
			zk = yk;
			yk = ak + (3.0 - sqrt(5.0)) / 2.0 * (bk - ak);
		}
		else
		{
			ak = yk;
			yk = zk;
			zk = ak + (sqrt(5.0) - 1.0) / 2.0 * (bk - ak);
		};

		if (DEBUG_GOLDEN) addDataInFile(fileD, "ak", ak);
		if (DEBUG_GOLDEN) addDataInFile(fileD, "bk", bk);

		count++;
	};

	return (ak + bk) / 2.0;
}

double FibbonachCoeff(int index)
{
	if ((index == 0) || (index == 1))
		return 1.0;

	double fb_1 = 1.0;
	double fb_2 = 1.0;
	double fb_3 = 1.0;
	for (int i = 2; i <= index; i++)
	{
		fb_1 = fb_2;
		fb_2 = fb_3;
		fb_3 = fb_1 + fb_2;
	};

	return fb_3;
}

double FibbonachMethod(double a, double b, double epsilon, int& iteration)
{
	int count = 0;
	double ak = a;
	double bk = b;

	if (DEBUG_FIBBON) addDataInFile(fileF, "ak", ak);
	if (DEBUG_FIBBON) addDataInFile(fileF, "bk", bk);

	int N = 0;
	while (FibbonachCoeff(N + 2) < ((b - a) / epsilon))
		N++;

	double yk = a + FibbonachCoeff(N) / FibbonachCoeff(N + 2) * (b - a);
	double zk = a + FibbonachCoeff(N + 1) / FibbonachCoeff(N + 2) * (b - a);
	
	while (count != (N - 1))
	{
		if (func(yk) <= func(zk))
		{
			bk = zk;
			zk = yk;
			yk = ak + FibbonachCoeff(N - count) / FibbonachCoeff(N - count + 2) * (bk - ak);
		}
		else
		{
			ak = yk;
			yk = zk;
			zk = ak + FibbonachCoeff(N - count + 1) / FibbonachCoeff(N - count + 2) * (bk - ak);
		};

		if (DEBUG_FIBBON) addDataInFile(fileF, "ak", ak);
		if (DEBUG_FIBBON) addDataInFile(fileF, "bk", bk);

		count++;
	};

	iteration = N - 1;

	return (ak + bk) / 2.0;
}

int main()
{
	double a = 2.0;   // Нижняя граница поиска решения для метода Свена
	double b = 100.0; // Верхняя граница поиска решения для метода Свена

	double xOld = 90.0;  // Произвольная точка для метода Свенна
	double step = 0.5; // Шаг сетки (для построения графика функции)

	if (DEBUG_MAIN) saveGraphFile(leftBorder, rightBorder, step); // Построить график исследуемой функции

	VennanMethod(a, b, xOld, step); // Метод Свенна
	
	std::string fileName = "0.00001.dat"; // Название файла с результатами расчётов
	int iteration;                        // Переменная для хранения числа итераций по каждому из методов
	double epsilon = 0.00001;             // Число epsilon для расчётов
	double x = 0.0;                       // Искомая точка x

	iteration = 0; x = DichotomyMethod(a, b, epsilon, iteration);   addPointInFile(fileName, x, iteration);
	iteration = 0; x = GoldenRatioMethod(a, b, epsilon, iteration); addPointInFile(fileName, x, iteration);
	iteration = 0; x = FibbonachMethod(a, b, epsilon, iteration);   addPointInFile(fileName, x, iteration);

	return 0;
}